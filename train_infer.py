
from simple_cnn import Net
import torch.nn as nn
import torch.optim as optim
from torch_utils import *


train_loader, test_loader = get_cifar_loaders()


# Load the net and define the optimizer
net = Net()
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)


def train_fcn(epochs):
    loss_history = []
    correct_history = []
    correct = 0
    total = 0

    for epoch in range(epochs):  # loop over the dataset multiple times
        running_loss = 0.0
        for i, data in enumerate(train_loader, 0):
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = data

            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            _, predicted = torch.max(outputs.data, 1)
            correct += (predicted == labels).sum().item()
            total += labels.size(0)

            # print statistics
            running_loss += loss.item()
            if i % 2000 == 1999:  # print every 2000 mini-batches
                print('[%d, %5d] loss: %.3f' %
                      (epoch + 1, i + 1, running_loss / 2000))
                loss_history.append(running_loss / 2000)
                correct_history.append(correct / total)
                total = 0
                running_loss = 0.0
    return loss_history, correct_history


def infer_fcn(mlflow_model):
    mlflow_model.eval()
    correct = 0
    total = 0
    running_loss = 0.0

    for i, data in enumerate(train_loader, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data

        # Run inputs through model and calculate loss/accuracy
        outputs = mlflow_model(inputs)
        loss = criterion(outputs, labels)

        _, predicted = torch.max(outputs.data, 1)
        correct += (predicted == labels).sum().item()
        total += labels.size(0)
        running_loss += loss.item()

    return running_loss / total, correct / total

