
# This CNN code is adapted from: https://pytorch.org/tutorials/beginner/blitz/cifar10_tutorial.html
# The purpose of the toy project is to show an end to end example of training a CNN with basic features of
# MLFlow highlighted.

import os
from mlflow_utils import MLFlowUtils
import mlflow.pytorch
from train_infer import *


def train_model():
    # Path to save model to
    save_base_path = "../../models/"
    None if os.path.exists(save_base_path) else os.makedirs(save_base_path)

    # Get MLFlow ready to go
    flow = MLFlowUtils()

    # Commit changes made to files
    flow.git_commit(commit_str="Training run from mlflow_example.py")
    run_hash = flow.get_git_hash(hash_bits=8)

    # Start training and logging for this run (anything logged outside of the 'with' will be stored under a
    # different entry in the ui.)
    with mlflow.start_run() as mlflow_run:
        loss_history, accuracy_history = train_fcn(1)

        mlflow.pytorch.log_model(net, artifact_path=run_hash)  # model_repo_path
        flow.save_model_path_proj_ts(mlflow_run, base_save_path=save_base_path)

        for idx, (loss, accuracy) in enumerate(zip(loss_history, accuracy_history)):
            mlflow.log_metrics({"loss": loss, "accuracy": accuracy}, step=idx)

        flow.save_plt_artifact(lines_list=[accuracy_history, loss_history], line_labels=["Accuracy", "Loss"],
                               save_name="training_plots.png")

    print('Finished Training')
    pass


def infer_model():
    # Get MLFlow ready to go
    flow = MLFlowUtils()

    # model_path = "./mlruns/0/fc46b7dface147af967290cbfe6e31f5"
    model_path = "D:\\models\\2021_11_11\\bcdde6a4"

    loaded_model = flow.load_model_from_path(model_path)

    # Commit changes made to files
    flow.git_commit(commit_str="Inference run")
    run_hash = flow.get_git_hash(hash_bits=8)

    # Start mlflow capture
    with mlflow.start_run() as mlflow_run:
        # Run test data through loaded model
        mlflow.log_param("Parameter", "Inference run %s" % run_hash)
        loss, accuracy = infer_fcn(loaded_model)
        mlflow.log_metrics({"loss": loss, "accuracy": accuracy})

    print("loss: ", loss)
    print("accuracy: ", accuracy)
    print("git hash: ", run_hash)
    pass


if __name__ == "__main__":
    # train_model()
    infer_model()

